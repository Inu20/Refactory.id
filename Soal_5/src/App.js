import './App.css';
import NavbarComp from './component/NavbarComp';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-calendar/dist/Calendar.css';
import CardComp from './component/CardComp';
import CalendarComp from './component/CalendarComp';
import TaskComp from './component/TaskComp';
import { Col, Row } from 'react-bootstrap';



function App() {
  return (
    <div className="App">
      <header>
        <NavbarComp />
      </header>
      <body>
        <Row>
          <Col md={4} style={{ paddingLeft:"49px"}}>
            <CardComp />
            <CalendarComp />
          </Col>
          <Col md={7} style={{paddingLeft:'45px'}}>
            <TaskComp />
          </Col>
        </Row>
      </body>
    </div>
  );
}

export default App;
