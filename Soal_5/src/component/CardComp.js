import React from 'react'
import { Card, Row, Col } from 'react-bootstrap'
import './Comp.css'
import Todo from './img/to-do.svg'

export default function CardComp() {
    return (
        <div className="Card-Wrapper">
            <Card id="CardNotif">
                <Card.Body>
                    <Row>
                        <Col md={4}>
                            <img src={Todo} />
                        </Col>
                        <Col md={8}>
                            <h3>Hallo, Sam!</h3>
                            <p>You have 2 tasks left for today, Already completed Task today?</p>
                        </Col>
                    </Row>

                </Card.Body>
            </Card>
        </div>
    )
}
