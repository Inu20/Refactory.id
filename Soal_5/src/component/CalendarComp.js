import React, { useState } from 'react';
import Calendar from 'react-calendar';
import './Comp.css'

export default function CalendarComp() {
    const [value, onChange] = useState(new Date());

    return (
        <div className="Calender-Wrapp">
            <Calendar
                onChange={onChange}
                value={value}
            />
        </div>
    )
}
