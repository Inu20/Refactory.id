import React, { useState } from 'react'
import { Row, Col, Button, Form, FormControl, Card, Badge, Modal } from 'react-bootstrap'
import Calendar from 'react-calendar';
import Plus from './img/plus.png'
import Edit from './img/edit.png'
import Delete from './img/trash.png'




export default function TaskComp() {
    const [ModalShow, setModalShow] = useState(false);
    const [value, onChange] = useState(new Date());

    return (
        <div className="Task-Wrapper">
            <Row>
                <Col md={11}>
                    <div className="Task-Tittle">
                        <h1>Task For Today</h1>
                    </div>
                </Col>
                <Col>
                    <div className="Task-Form">
                        <Form>
                            <Row>
                                <Col md={3}>
                                    <div className="btn-create">
                                        <Button onClick={() => setModalShow(true)}> <img src={Plus} /> Create New </Button>
                                    </div>
                                </Col>
                                <Col md={5}>
                                    <div className="task-search">
                                        <FormControl type="text" placeholder="Cari" className="text-center" />
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <Card className="card-task">
                        <Card.Body>
                            <div className="content-task">
                                <h1> Daily Stand Up <span><Badge style={{ fontSize: '10px', color: '#FB3453', backgroundColor: '#FEEAEB', borderRadius: '5px' }}>Complete</Badge></span></h1>
                                <p>1. Dont forget prepare your report and any blocker in Front end.<br />
                                2. Telling Project Manager for new assignment.
                                </p>
                            </div>
                            <div className="edit-task">
                                <img src={Delete} />
                                <img src={Edit} />
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={12}>
                    <Card className="card-task">
                        <Card.Body>
                            <div className="content-task">
                                <h1> Daily Stand Up <span><Badge style={{ fontSize: '10px', color: '#D3D5DB', backgroundColor: '#F2F4F8', borderRadius: '5px' }}> Set As Complete</Badge></span></h1>
                                <p>1. Dont forget prepare your report and any blocker in Front end.<br />
                                2. Telling Project Manager for new assignment.
                                </p>
                            </div>
                            <div className="edit-task">
                                <img src={Delete} />
                                <img src={Edit} />
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={12}>
                    <Card className="card-task">
                        <Card.Body>
                            <div className="content-task">
                                <h1> Daily Stand Up <span><Badge style={{ fontSize: '10px', color: '#D3D5DB', backgroundColor: '#F2F4F8', borderRadius: '5px' }}> Set As Complete</Badge></span></h1>
                                <p>1. Dont forget prepare your report and any blocker in Front end.<br />
                                2. Telling Project Manager for new assignment.
                                </p>
                            </div>
                            <div className="edit-task">
                                <img src={Delete} />
                                <img src={Edit} />
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

            <div className="modal-task">
                <Modal
                    size="md"
                    show={ModalShow}
                    onHide={() => setModalShow(false)}
                    aria-labelledby="example-modal-sizes-title-md"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="example-modal-sizes-title-md">
                            New Task
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group>
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="text" placeholder="Fill title" />
                            </Form.Group>
                            <Form.Group >
                                <Form.Label>Description</Form.Label>
                                <Form.Control as="textarea" rows={3} placeholder="Fill description" />
                            </Form.Group>
                            <Form.Group >
                                <Form.Label> Choose Date</Form.Label>
                                <Calendar
                                    onChange={onChange}
                                    value={value}
                                />
                            </Form.Group>
                            <Modal.Footer>
                                <div className="btn-taskcancel">
                                <Button onClick={() => setModalShow(false)}>
                                    Cancel
                                </Button>
                                </div>
                                <div className="btn-task">
                                    <Button onClick={() => setModalShow(false)}>
                                        Create Task
                                </Button>
                                </div>
                            </Modal.Footer>
                        </Form>
                    </Modal.Body>
                </Modal>
            </div>
        </div>
    )
}
