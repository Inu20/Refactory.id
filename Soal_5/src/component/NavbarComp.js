import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import Avatar from './img/Avatar.png'
import Setting from './img/Setting.png'
import './Comp.css'

export default function NavbarComp() {
    return (
        <div>
            <Navbar  style={{backgroundColor:'#0442D0'}} expand="lg">
                <Navbar.Brand href="#" style={{color:'white', marginLeft:'44px', fontFamily:'Roboto', fontSize:'30px'}}> Logo </Navbar.Brand>
                    <Nav className="ml-auto">
                        <Navbar.Text style={{color:'white'}}> <span> <img style={{width:'52px', height:'52px', marginRight:'21px'}} src={Avatar} alt="Avatar" /> </span> Samsul Arifin <span> <img style={{width:'24px', height:'24px', marginLeft:'30px'}}src={Setting}/> </span> </Navbar.Text>
                    </Nav>
            </Navbar>
        </div>
    )
}
